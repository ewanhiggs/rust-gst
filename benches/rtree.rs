#![feature(test)]
extern crate rand;
extern crate test;
extern crate gst;
use gst::rtree::{RTree, Rect};
use rand::thread_rng;
use rand::distributions::{IndependentSample, Range};
use test::Bencher;

#[bench]
fn bench_rtree_inserts(b: &mut Bencher) {
    let mut rng = thread_rng();
    let mut rtree = RTree::new();
    let between = Range::new(0f32, 200f32);
    let vals = (0..100_000).map(|_| {
        let x = between.ind_sample(&mut rng);
        let y = between.ind_sample(&mut rng);
        (x, y)
    }).collect::<Vec<_>>();
    let mut vals_iter = vals.into_iter();
    let word = "Hello".to_string();
    b.iter(|| {
        let (x, y) = vals_iter.next().unwrap();
        rtree.insert(Rect::from_float(x, x, y, y), word.clone());
    });
}

#[bench]
fn bench_rtree_lookups_get_rect(b: &mut Bencher) {
    let mut rng = thread_rng();
    let mut rtree = RTree::new();
    let between = Range::new(-100f32, 100f32);
    (0..100_000).map(|i| {
        let x = between.ind_sample(&mut rng);
        let y = between.ind_sample(&mut rng);
        rtree.insert(Rect::from_float(x, x, y, y), format!("{}", i));
    }).collect::<Vec<_>>();

    let vals = (0..10_000).map(|_| {
        let x = between.ind_sample(&mut rng);
        let y = between.ind_sample(&mut rng);
        (x, y)
    }).collect::<Vec<_>>();
    let mut vals_iter = vals.into_iter().cycle();
    let mut last = vec![];
    b.iter(|| {
        let (x, y) = vals_iter.next().unwrap();
        last = rtree.get(&Rect::from_float(x-5., x+5., y-5., y+5.));
    });
}

